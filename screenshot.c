#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>

// sudo apt install libx11-dev -y
// gcc screenshot.c -o screenshot -lX11

void SaveScreenshot(Display* display, Window root, int x, int y, int width, int height) {
    XImage* img = XGetImage(display, root, x, y, width, height, AllPlanes, ZPixmap);
    int bytes_per_line = img->bytes_per_line;

    time_t current_time;
    struct tm* time_info;
    char time_str[31];
    time(&current_time);
    time_info = localtime(&current_time);
    strftime(time_str, 31, "screenshot_%Y%m%d_%H%M%S.ppm", time_info);

    FILE* fp = fopen(time_str, "wb");
    if (!fp) {
        perror("Failed to open file for writing");
        XDestroyImage(img);
        return;
    }

    fprintf(fp, "P6\n%d %d\n255\n", width, height);

    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            unsigned long pixel = XGetPixel(img, j, i);
            fputc((pixel & 0x00FF0000) >> 16, fp); // Red
            fputc((pixel & 0x0000FF00) >> 8, fp);  // Green
            fputc(pixel & 0x000000FF, fp);         // Blue
        }
    }

    fclose(fp);
    XDestroyImage(img);
}

int main() {
    Display* display = XOpenDisplay(NULL);
    if (!display) {
        fprintf(stderr, "Failed to open X display\n");
        return 1;
    }

    Window root = DefaultRootWindow(display);
    int screen = DefaultScreen(display);
    int width = DisplayWidth(display, screen);
    int height = DisplayHeight(display, screen);

    SaveScreenshot(display, root, 0, 0, width, height);

    XCloseDisplay(display);
    return 0;
}
